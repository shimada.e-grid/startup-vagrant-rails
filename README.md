# startup-vagrant-rails

1. `vagrant plugin install vagrant-docker-compose`
2. `vagrant up`
3. `docker-compose run web bash`
4. `rails new . --database=postgresql --skip-coffee --skip-sprockets --skip-turbolinks`
